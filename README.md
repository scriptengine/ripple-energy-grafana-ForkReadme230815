# Ripple Energy Dashboard

To add this dashboard to Grafana in Home Assistant 

You will need:
- Home Assistant (of course)
- Grafana installed and configured on Home Assistant
[ Both these are not covered in this readme]

- The JSON from this repository (instructions below)
- A Ripple Energy User API 
See https://community.rippleenergy.com/new-feature-requests-yyqtfatb/post/ripple-api-yH0cTzuQ4GJMaYV

- The Grafana Infinity plug in <https://sriramajeyam.com/grafana-infinity-datasource/>
[The instructions to install this are below]

On HA in Grafana go to
Home>Connections>Add new connection
Search for Infinity
Click Infinity
Install (blue button top right)

Create a default Infinity data source (just save and test)

Now add the dashboard
Download the json from: https://gitlab.com/twem/ripple-energy-grafana/-/blob/master/dasboard.json?ref_type=heads

(In Grafana)
Home>Dashboards
New>Import
Drop json onto "upload dashboard JSON file" (or import using the file dialog)
Select Infinity as the data source
Add your own API key
Click Import